#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rapidjson/rapidjson.h"
#include <iostream>
#include <cstdio>
#include <map>
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>
using namespace std;
double toRadians(double degree) {
    return degree * M_PI / 180;
}

double getDistance(double lon1, double lat1, double lon2, double lat2) {
    double r = 6378137.0;   // earth radius in meter

    lat1 = toRadians(lat1);
    lon1 = toRadians(lon1);
    lat2 = toRadians(lat2);
    lon2 = toRadians(lon2);

    double dlat = lat2 - lat1;
    double dlon = lon2 - lon1;

    double d = 2 * r * asin(sqrt(pow(sin(dlat / 2), 2) + cos(lat1) * cos(lat2) * pow(sin(dlon / 2), 2)));

    return d/1000.0;
}
using namespace rapidjson;
map<string,Document> mp;

int main() {
    string p[11]={"parks","schools","hospa","hospc","clina","clinc","vet","sport","mfc","busst","metro"};
    Document addr;
    for(int ii=0;ii<11;ii++){
        string s=p[ii];
        mp.insert(make_pair(s,Document()));
    }
    char buff[65536];
    FILE* f = fopen("done_data/processed.json", "w");
    FILE *ff=fopen("done_data/addrs.json", "rb");
    cout << "[DONE] done_data/addrs.json" << endl;
    FileReadStream iss(ff, buff, sizeof(buff));
    addr.ParseStream(iss);
    for(int ii=0;ii<11;ii++){
        string s=p[ii];
        FILE *fp=fopen(("done_data/"+s+".json").c_str(), "rb");
        cout << "[DONE] done_data/"+s+".json" << endl;
        FileReadStream is(fp, buff, sizeof(buff));
        mp[s].ParseStream(is);
    }

    int cc=0;
    int len=addr.Size();
    for(auto &add: addr.GetArray()){
        Value res(kObjectType);
        Document doc;
        doc.Parse("[]");
        for(int ii=0;ii<11;ii++){
            string j=p[ii];
            vector<pair<double,int>> dists;
            auto* sp=&mp[j];
            int k=0;
            for(auto &i: sp->GetArray()){
                if(string(i[0].GetString())=="Point") {
                    dists.push_back(make_pair(getDistance(i[1][0].GetDouble(),i[1][1].GetDouble(), add[0][0].GetDouble(), add[0][1].GetDouble()), k++));
                    continue;
                }
                if(string(i[0].GetString())=="MultiPoint"){
                    double min_d=10000;
                    for (auto& l : i[1].GetArray())
                        min_d=min(getDistance(l[0].GetDouble(),l[1].GetDouble(),add[0][0].GetDouble(),add[0][1].GetDouble()),min_d);
                    dists.push_back(make_pair(min_d,k++));
                    continue;
                }
                if(string(i[0].GetString())=="Polygon"){
                    double min_d=10000;
                    for (auto& l : i[1][0].GetArray())
                        min_d=min(getDistance(l[0].GetDouble(),l[1].GetDouble(),add[0][0].GetDouble(),add[0][1].GetDouble()),min_d);
                    dists.push_back(make_pair(min_d,k++));
                    continue;
                }
                if(string(i[0].GetString())=="MultiPolygon"){
                    double min_d=10000;
                    for (auto& ll : i[1].GetArray()){
                        for (auto&l : ll[0].GetArray())
                            min_d=min(getDistance(l[0].GetDouble(),l[1].GetDouble(),add[0][0].GetDouble(),add[0][1].GetDouble()),min_d);
                    }
                    dists.push_back(make_pair(min_d,k++));
                }
            }
            sort(dists.begin(),dists.end());
            int d=0;
            Value dis(kArrayType);
            for (auto val: dists){
                Value pair_(kArrayType);
                pair_.PushBack(Value(val.first).Move(),doc.GetAllocator());
                pair_.PushBack(Value(val.second).Move(),doc.GetAllocator());
                dis.PushBack(pair_.Move(),doc.GetAllocator());
                d++;
                if(d==10)
                    break;
            }
            Value key;
            key.SetString(j.c_str(),doc.GetAllocator());

            res.AddMember(key.Move(),dis.Move(),doc.GetAllocator());
        }
        doc.PushBack(res.Move(),doc.GetAllocator());
        rapidjson::StringBuffer buffer;

        buffer.Clear();

        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        writer.SetMaxDecimalPlaces(3);
        doc[0].Accept(writer);
        fprintf(f,"%s",buffer.GetString());
        if(cc!=len-1)
            fprintf(f,"\n");
        fflush(f);
        cout << cc+1 << "\r";
        cout.flush();
        doc.Erase(doc.Begin());
        cc++;
    }
}