#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rapidjson/rapidjson.h"
#include <iostream>
#include <cstdio>
#include <map>
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
using namespace std;
using namespace rapidjson;
map<string,Document> mp;
vector<Document>pr;
using namespace std::chrono;
int main(int argc, char** argv) {
    string p[11]={"parks","schools","hospa","hospc","clina","clinc","vet","sport","mfc","busst","metro"};
    string req;
    double val;
    if(argc!=3){
        cout << "2 arguments required\n"
                "generate_report utility\n"
                "Part of %NameHere% project\n"
                "Usage: generate_report [datatype] [value]\n"
                "[datatype]: string\n"
                "Possible values: parks, schools, hospa, hospc, clina, clinc, vet, sport, mfc, busst, metro\n"
                "[value]: double\n"
                "Possible values: any double type number\n";
        return 1;
    }
    req=string(argv[1]);
    bool arg=false;
    for (int i = 0; i < 11; ++i) {
        if(string(argv[1])==p[i]){
            arg=true;
            break;
        }

    }
    if(!arg)
    {
        cout << "Invalid argument\n"
                "generate_report utility\n"
                "Part of %NameHere% project\n"
                "Usage: generate_report [datatype] [value]\n"
                "[datatype]: string\n"
                "Possible values: parks, schools, hospa, hospc, clina, clinc, vet, sport, mfc, busst, metro\n"
                "[distance(meters)]: integer\n"
                "Possible values: any integer type number\n";
        return 1;
    }
    try{
        val=stod(string(argv[2]))/1000.0;
    }
    catch (int e){
        cout << "Invalid argument\n"
                "generate_report utility\n"
                "Part of %NameHere% project\n"
                "Usage: generate_report [datatype] [value]\n"
                "[datatype]: string\n"
                "Possible values: parks, schools, hospa, hospc, clina, clinc, vet, sport, mfc, busst, metro\n"
                "[distance(meters)]: integer\n"
                "Possible values: any integer type number\n";;
        return 1;
    }
    stringstream stt;
    stt << string(argv[1])+" "+string(argv[2]);
    string comm="mkdir \"../reports/" +string(argv[1])+" "+string(argv[2]) + "\"";
    system(comm.c_str());
    system(string("cp ./report_web/map.html \"../reports/"+stt.str()+"/map.html\"").c_str());
    system(string("cp ./report_web/heatLayer.js \"../reports/"+stt.str()+"/heatLayer.js\"").c_str());
    stringstream result;
    result << "var ProcessedPoints=[";
    Document s;
    string ss;
    Document addr;
    for(int ii=0;ii<11;ii++){
        string s=p[ii];
        mp.insert(make_pair(s,Document()));
    }
    char buff[65536];
    ifstream fn("done_data/processed.json");
    while (getline(fn,ss)) {
        pr.push_back(Document());
        pr[pr.size()-1].Parse(ss.c_str());
    }
    cout << "[DONE] processed.json" << endl;
    FILE *ff=fopen("done_data/addrs.json", "rb");
    FileReadStream iss(ff, buff, sizeof(buff));
    addr.ParseStream(iss);
    cout << "[DONE] done_data/addrs.json" << endl;
    for(int ii=0;ii<11;ii++){
        string s=p[ii];
        FILE *fp=fopen(("done_data/"+s+".json").c_str(), "rb");
        FileReadStream is(fp, buff, sizeof(buff));
        mp[s].ParseStream(is);
        cout << "[DONE] done_data/"+s+".json" << endl;
    }
    int kk=0;
    bool comma=false;
    for(int i=0;i<pr.size();i++){
        Document *ip=&pr[i];
        if((*ip)[req.c_str()].GetArray()[0].GetArray()[0].GetDouble()>val) {
            if (comma) {
                result << ",";
            } else comma = true;
            result << "[" << addr[i][0][1].GetDouble() << "," << addr[i][0][0].GetDouble() << ",'"
                   << addr[i][1].GetString() << "']";
            kk++;
        }
    }
    result << "];";
    ofstream of("../reports/"+stt.str()+"/report.js");
    of << result.str();
    of.close();
    ofstream rep("../reports/"+stt.str()+"/report.txt");
    rep << "Chosen: " << kk << " out of " <<pr.size() <<"\n"
           <<kk/double(pr.size())*100.0<<"%";
    rep.close();
}