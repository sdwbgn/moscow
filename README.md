# moscow
## Executables
Executables are compiled on macOS 10.14.
## Files
- **data** folder includes data from data.mos.ru
- **reports** folder includes reports based on given data
- **src** folder includes source files of *distance_calc* and *generate_report*
- **data/parse_data.py** - generates ready-to-process data
- **data/distance_calc** - processes data
- **data/generate_report** - generates report with map in *reports* folder
## Abbreviations
- **busst** - Bus Stops
- **metro** - Subway Stops
- **clina** - Adult Clinics
- **clinc** - Children Clinics
- **hospa** - Adult Hospitals
- **hospc** - Children Hospitals
- **parks** - Park Territories
- **vet** - Veterinary Clinics
- **mfc** - Govermental Services Institutions
- **schools** - Schools
## Report folder format
*"[datatype]* *[distance in meters]"*
## Dependencies
- Python 3.7.0