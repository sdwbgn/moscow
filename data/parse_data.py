import json
import codecs
import os
## Addresses Parsing
def addrs():
    coord_db=[]
    for k in range(1,7):
        r=json.loads(codecs.open("./addrs/"+str(k)+".json","r","cp1251").read())
        for i in r:
            try:
                coord_db.append([i['geoData']['center'][0],i['ADDRESS'],i['geoData']['type'],i['geoData']['coordinates']])
            except:
                do=False
    os.makedirs(os.path.dirname("./done_data/addrs.json"), exist_ok=True)
    f=open("./done_data/addrs.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Addresses Parsed");

## Parks Parsing
def parks():
    coord_db=[]
    r=json.loads(codecs.open("./parks/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['CommonName']])
    os.makedirs(os.path.dirname("./done_data/parks.json"), exist_ok=True)
    f=open("./done_data/parks.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Parks Parsed");

## Schools Parsing
def schools():
    coord_db=[]
    r=json.loads(codecs.open("./schools/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['FullName'],i['ShortName'],i['LegalAddress']])
    os.makedirs(os.path.dirname("./done_data/schools.json"), exist_ok=True)
    f=open("./done_data/schools.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Schools Parsed");

## Adult Hospitals parsing
def hospa():
    coord_db=[]
    r=json.loads(codecs.open("./hospa/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['FullName'],i['ShortName'],'город Москва, '+i['ObjectAddress'][0]['District']+', '+i['ObjectAddress'][0]['Address']])
    os.makedirs(os.path.dirname("./done_data/hospa.json"), exist_ok=True)
    f=open("./done_data/hospa.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Adult Hospitals Parsed");

## Child Hospitals parsing
def hospc():
    coord_db=[]
    r=json.loads(codecs.open("./hospc/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['FullName'],i['ShortName'],'город Москва, '+i['ObjectAddress'][0]['District']+', '+i['ObjectAddress'][0]['Address']])
    os.makedirs(os.path.dirname("./done_data/hospc.json"), exist_ok=True)
    f=open("./done_data/hospc.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Children Hospitals Parsed");

## Adult Clinics parsing
def clina():
    coord_db=[]
    r=json.loads(codecs.open("./clina/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['FullName'],i['ShortName'],'город Москва, '+i['ObjectAddress'][0]['District']+', '+i['ObjectAddress'][0]['Address']])
    os.makedirs(os.path.dirname("./done_data/clina.json"), exist_ok=True)
    f=open("./done_data/clina.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Adult Clinics Parsed");

## Child Clinics parsing
def clinc():
    coord_db=[]
    r=json.loads(codecs.open("./clinc/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['FullName'],i['ShortName'],'город Москва, '+i['ObjectAddress'][0]['District']+', '+i['ObjectAddress'][0]['Address']])
    os.makedirs(os.path.dirname("./done_data/clinc.json"), exist_ok=True)
    f=open("./done_data/clinc.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Children Clinics Parsed");
    
## Subway stations parsing
def metro():
    coord_db=[]
    r=json.loads(codecs.open("./metro/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['NameOfStation']])
    os.makedirs(os.path.dirname("./done_data/metro.json"), exist_ok=True)
    f=open("./done_data/metro.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Subway Stations Parsed");

## Bus Stops parsing
def busst():
    coord_db=[]
    r=json.loads(codecs.open("./busst/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['stop_name']])
    os.makedirs(os.path.dirname("./done_data/busst.json"), exist_ok=True)
    f=open("./done_data/busst.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Bus Stops Parsed");

## Veterinary Clinics parsing
def vet():
    coord_db=[]
    r=json.loads(codecs.open("./vet/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['FullName'],i['ShortName'],'город Москва, '+i['Address']])
    os.makedirs(os.path.dirname("./done_data/vet.json"), exist_ok=True)
    f=open("./done_data/vet.json","w")
    f.write(json.dumps(coord_db))
    f.close()

## Sport Buildings parsing
def sport():
    coord_db=[]
    r=json.loads(codecs.open("./sport/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['FullName'],i['ShortName'],'город Москва, '+i['ObjectAddress'][0]['District']+', '+i['ObjectAddress'][0]['Address']])
    os.makedirs(os.path.dirname("./done_data/sport.json"), exist_ok=True)
    f=open("./done_data/sport.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Sport Building Parsed");

## Gov. Services Buildings parsing
def mfc():
    coord_db=[]
    r=json.loads(codecs.open("./mfc/1.json","r","cp1251").read())
    for i in r:
        coord_db.append([i['geoData']['type'],i['geoData']['coordinates'],i['FullName'],i['ShortName'],['Address']])
    os.makedirs(os.path.dirname("./done_data/mfc.json"), exist_ok=True)
    f=open("./done_data/mfc.json","w")
    f.write(json.dumps(coord_db))
    f.close()
    print("[DONE] Gov. Services Buildings Parsed");

## Sequentially parsing
addrs()
parks()
schools()
hospa()
hospc()
clina()
clinc()
metro()
busst()
vet()
sport()
mfc()
